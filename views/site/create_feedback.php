<?php

use app\models\CreateFeedbackForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $feedback CreateFeedbackForm*/

$this->title = 'Create Feedback';
?>

<div class="reviews-form">

    <h1><?= Html::encode($this->title) ?></h1>

    <!--<div class="alert alert-success col-lg-12" role="alert">
        <?/*= Yii::$app->session->getFlash('feedback'); */?>
    </div>-->

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($feedback, 'username')->textInput([
        'maxlength' => true,
        'value' => Yii::$app->user->identity->username,
        'readonly' => true,
    ]) ?>

    <?= $form->field($feedback, 'feedback_text')->textarea(['rows' => 10]) ?>

    <?/*= $form->field($model, 'date')->widget(DateControl::classname(), [
        'type' => 'date',
    ]) */?>

    <?/*= $form->field($feedback, 'status_active')->dropDownList([0 => 'неактивный', 1 => 'активный']) */?>

    <?/*= $form->field($model, 'image')->fileInput() */?><!--
    <button>Load</button>-->
    <?= $form->field($feedback, 'image')->fileInput() ?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
