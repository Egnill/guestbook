<?php

namespace app\models;

use Yii;
use yii\base\Model;

class CommentForm extends Model
{
    public $id;
    public $feedbackId;
    public $userId;
    public $commentText;
    public $date;

    public $image;
    public $path;

    public function rules()
    {
        return [
            [['feedbackId', 'userId', 'commentText', 'date'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'feedbackId' => 'Feedback Id',
            'userId' => 'User Id',
            'commentText' => 'Comment',
            'date' => 'Date',
        ];
    }

    public function saveComment($id) {

        $comment = new Comment();

        $comment->feedbackId = $id;
        $comment->userId = Yii::$app->user->identity->getId();
        $comment->commentText = $this->commentText;
        $comment->date = date('Y-m-d H:i', time());

        return $comment->save(false) ? $comment : null;
    }

    public function saveImg($id) {

        $commentImg = new CommentImg();

        if ($this->image !== null) {
            $this->path = "upload/{$this->image->baseName}.{$this->image->extension}";
            $this->image/*->resize(new Box(500, 300))*/->saveAs($this->path);

            $commentImg->commentId = $id;
            $commentImg->path = $this->path;

            return $commentImg->save(false);
        }

        return null;
    }
}