<?php


namespace app\models;

use Yii;
use yii\base\Model;

class CreateFeedbackForm extends Model
{
    public $id;
    public $userId;
    public $feedback_text;
    public $date;
    public $like;

    public $image;
    public $path;

    public function rules()
    {
        return [
            [['userId', 'feedback_text', 'date'], 'required'],
            [['date'], 'date', 'format' => 'Y-m-d H:i'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userId' => 'User Id',
            'feedback_text' => 'Feedback Text',
            'date' => 'Date',
            'like' => 'Like',
        ];
    }

    public function saveFeedback()
    {
        $feedback = new Feedback();
        $feedback->userId = Yii::$app->user->identity->getId();
        $feedback->feedback_text = $this->feedback_text;
        $feedback->date = date('Y-m-d H:i', time());
        return $feedback->save(false) ? $feedback : null;
    }

    public function saveImg($id) {

        $feedbackImg = new FeedbackImg();

        if ($this->image !== null) {
            $this->path = "upload/{$this->image->baseName}.{$this->image->extension}";
            $this->image/*->resize(new Box(500, 300))*/->saveAs($this->path);

            $feedbackImg->feedbackId = $id;
            $feedbackImg->path = $this->path;

            return $feedbackImg->save(false);
        }

        return null;
    }

}