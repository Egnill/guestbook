<?php

namespace app\controllers;

use app\models\Comment;
use app\models\CommentForm;
use app\models\CreateFeedbackForm;
use app\models\Feedback;
use app\models\FeedbackImg;
use app\models\RegistrationForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use yii\web\UploadedFile;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'isGuest' => Yii::$app->user->isGuest,
            'feedbacks' => Feedback::find()->all(),
        ]);
    }

    /**
     * Displays create page.
     *
     * @return string|Response
     */
    public function actionCreate() {

        $feedback = new CreateFeedbackForm();

        if ($feedback->load(Yii::$app->request->post())) {
            $feedback->image = UploadedFile::getInstance($feedback, 'image');
            //$feedback->saveFeedback();
            $feedback->saveImg($feedback->saveFeedback()->id);
            return $this->redirect('index');
        }

        return $this->render('create_feedback', [
            'feedback' => $feedback,
        ]);
    }

    /*public function actionEdit($id) {

        $feedback = Feedback::getFeedback($id);

        if (Yii::$app->request->isPost) {

        }

        return $this->render('edit_feedback', [
            'feedback' => $feedback,
        ]);
    }*/

    /*public function actionUpload($id) {

        $feedbackImg = new FeedbackImgForm();

        $feedbackImg->load(Yii::$app->request->post());
        $feedbackImg->saveImg($id);

        return $this->refresh('create_feedback');
    }*/

    public function actionLike($id) {

        $feedback = Feedback::findOne(['id' => $id]);

        $feedback->likeCount();

        return $this->actionIndex();
    }

    public function actionLikeInShow($id) {
        $feedback = Feedback::findOne(['id' => $id]);

        $feedback->likeCount();
        return $this->actionShowFeedback($id);
    }

    public function actionLikeComment($id) {

        $comment = Comment::findOne(['id' => $id]);
        $comment->like();

        return $this->actionShowFeedback($comment->feedbackId);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Registration action.
     *
     * @return Response|string
     */
    public function actionRegistration() {

        $user = new RegistrationForm();

        if ($user->load(Yii::$app->request->post())) {
            //if ($user->validate()) {
                Yii::$app->user->login($user->signIn()/*, $this->rememberMe ? 3600*24*30 : 0*/);
            //}
            return $this->goBack();
        }

        return $this->render('registration', [
            'user' => $user,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionShowFeedback($id) {
        $feedback = Feedback::getFeedback($id);
        $feedbackImg = FeedbackImg::getAllPathById($id);

        $comment = new CommentForm();
        $comments = Comment::getComments($id);

        if (Yii::$app->request->isPost) {
            $comment->load(Yii::$app->request->post());
            $comment->image = UploadedFile::getInstance($comment, 'image');
            $comment->saveImg($comment->saveComment($id)->id);

            /*return $this->renderAjax('show_feedback', [
                'feedback' => $feedback,
                'comment' => $comment,
                'comments' => $comments,
                'feedbackImg' => $feedbackImg,
            ]);*/
            return $this->refresh('show_feedback');
        }

        return $this->render('show_feedback', [
            'feedback' => $feedback,
            'comment' => $comment,
            'comments' => $comments,
            'feedbackImg' => $feedbackImg,
        ]);
    }

    /**
     * Displays contact page.
     *
     * @return Response|string

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }*/

    /**
     * Displays about page.
     *
     * @return string

    public function actionAbout()
    {
        return $this->render('about');
    } */
}
