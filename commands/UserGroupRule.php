<?php


namespace app\commands;


use Yii;
use yii\rbac\Item;
use yii\rbac\Rule;

class UserGroupRule extends Rule
{
    public $name = 'userGroup';
    /**
     * @inheritDoc
     */
    public function execute($user, $item, $params)
    {
        if (!Yii::$app->user->isGuest) {
            $group = Yii::$app->user->identity->group;
            if ($item->name === 'admin') {
                return $group == 'admin';
            } elseif ($item->name === 'user') {
                return $group == 'user';
            }
        }
        return true;
    }
}