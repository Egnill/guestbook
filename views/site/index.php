<?php

/* @var $this yii\web\View */
/* @var $isGuest bool*/
/* @var $feedbacks Feedback*/

use app\models\Feedback;
use app\models\User;
use yii\helpers\Html;

$this->title = 'Guest Book';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Guest Book!</h1>

        <p class="lead">Here you can leave your feedback! Thanks for that!)</p>
    </div>

    <div class="col-8">
        <?php if (!$isGuest): ?>

            <div class="form-group">
                <?= Html::a('Send Feedback', ['/site/create'], ['class'=>'btn btn-primary']) ?>
            </div>
            <?php if (empty($feedbacks)): ?>
                <div class="align-content-center">
                    <p>There are no reviews yet)</p>
                </div>
            <?php else: ?>
                <?php foreach ($feedbacks as $note): ?>
                    <div class="container">
                        <div class="col">
                            <div class="col-lg-1">
                                <h4><?= Html::a(User::findById($note->userId)->username, ['show-feedback', 'id' => Html::encode($note->id)]) ?></h4>
                            </div>
                            <div class="col-lg-12">
                                <?= $note->feedback_text ?>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <?= $note->date ?>
                                </div>
                                <div class="col-lg-7"></div>
                                <div class="col-lg-1">
                                    <?= Html::a($note->like, ['/site/like', 'id' => Html::encode($note->id)])?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>
            <?php endif; ?>
        <?php endif; ?>
    </div>


</div>
