<?php


namespace app\commands;


use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {

        $authManager = Yii::$app->authManager;

        $guest = $authManager->createRole('guest');
        $admin = $authManager->createRole('admin');
        $user = $authManager->createRole('user');

        $login = $authManager->createPermission('login');
        $logout = $authManager->createPermission('logout');
        $error = $authManager->createPermission('error');
        $signUp = $authManager->createPermission('sign-up');
        $index = $authManager->createPermission('index');
        $view = $authManager->createPermission('view');
        $update = $authManager->createPermission('update');
        $delete = $authManager->createPermission('delete');

        $authManager->add($login);
        $authManager->add($logout);
        $authManager->add($error);
        $authManager->add($signUp);
        $authManager->add($index);
        $authManager->add($view);
        $authManager->add($update);
        $authManager->add($delete);

        $userGroupRule = new UserGroupRule();
        $authManager->add($userGroupRule);

        //guest
        $authManager->addChild($guest, $login);
        $authManager->addChild($guest, $logout);
        $authManager->addChild($guest, $error);
        $authManager->addChild($guest, $signUp);
        //$authManager->addChild($guest, $index);

        //user
        $authManager->addChild($user, $index);
        $authManager->addChild($user, $view);
        $authManager->addChild($user, $update);
        $authManager->addChild($user, $guest);

        //admin
        $authManager->addChild($admin, $delete);
        $authManager->addChild($admin, $user);
    }
}