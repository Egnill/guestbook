<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%feedback_img}}`.
 */
class m211005_083841_create_feedback_img_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%feedback_img}}', [
            'id' => $this->primaryKey(),
            'feedbackId' => $this->integer(),
            'path' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%feedback_img}}');
    }
}
