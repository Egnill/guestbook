<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property int $feedbackId
 * @property string $path
 * */
class FeedbackImg extends ActiveRecord
{
    public static function getAllPathById($id) {
        return FeedbackImg::find()->andWhere(['feedbackId' => $id])->one();
    }
}