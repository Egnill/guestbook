<?php


namespace app\models;


use Yii;
use yii\base\Model;

class RegistrationForm extends Model
{
    public $id;
    public $username;
    public $password;
    public $role;

    public $rememberMe = false;

    public function rules()
    {
        return [
            [['id', 'username', 'password', 'role'], 'required'],
            [['username', 'password', 'role'], 'string'],
            ['id', 'integer'],
            ['username', 'trim'],
            /*['username', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],*/
        ];
    }

    public function signIn() {

        $user = new User();

        $user->username = $this->username;
        $user->password = $this->password;
        $user->role = User::ROLE_USER;

        return $user->save() ? $user : null;
    }

}