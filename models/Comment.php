<?php


namespace app\models;

use yii\db\ActiveRecord;

/**
 *
 * @property int $id
 * @property int $feedbackId
 * @property int $userId
 * @property string $commentText
 * @property string $date
 * @property int $like
 * */
class Comment extends ActiveRecord
{
    public static function tableName()
    {
        return 'comment';
    }

    public function rules()
    {
        return [
            [['feedbackId', 'userId', 'commentText', 'date'], 'required'],
        ];
    }

    public function getAllComments() {
        return Comment::find()->all();
    }

    public function getComment($id) {
        return Comment::find()->andWhere(['id' => $id])->one();
    }

    public static function getComments($id) {
        return Comment::find()->andWhere(['feedbackId' => $id])->all();
    }

    public function like() {
        $this->like += 1;
        $this->save(false);
    }
}