<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%comment_img}}`.
 */
class m211006_095849_create_comment_img_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%comment_img}}', [
            'id' => $this->primaryKey(),
            'commentId' => $this->integer(),
            'path' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%comment_img}}');
    }
}
