<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property int commentId
 * @property string $path
 * */
class CommentImg extends ActiveRecord
{
    public static function getCommentImgById($id) {
        return CommentImg::find()->andWhere(['commentId' => $id])->one();
    }
}