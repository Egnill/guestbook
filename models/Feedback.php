<?php


namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "feedback".
 *
 *  @property int $id
 *  @property int $userId
 *  @property string $feedback_text
 *  @property string $date
 *  @property int $like
 * */

class Feedback extends ActiveRecord
{
    public static function tableName() {
        return 'feedback';
    }

    public function rules()
    {
        return [
            [['userId', 'feedback_text', 'date'], 'required'],
        ];
    }

    public function getFeedbackAll() {
        return Feedback::find()->all();
    }

    public static function getFeedback($id) {
        return new static(Feedback::find()->andWhere(['id' => $id])->one());
    }

    public function likeCount() {
        $this->like += 1;
        $this->save(false);
    }
}