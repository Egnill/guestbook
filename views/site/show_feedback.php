<?php


use app\models\Comment;
use app\models\CommentForm;
use app\models\CommentImg;
use app\models\Feedback;
use app\models\FeedbackImg;
use app\models\User;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $feedback Feedback */
/* @var $comment CommentForm */
/* @var $comments Comment */
/* @var $feedbackImg FeedbackImg*/
/* @var $commentImg CommentImg*/

$this->title = 'Feedback'
?>

<div class="site-feedback">
    <div class="container">
        <?php Pjax::begin() ?>
        <div class="row">
            <div class="col-lg-auto">
                <h3> <?= User::findById($feedback->userId)->username ?> </h3>
            </div>
        </div>
        <div class="row">
            <?= $feedback->feedback_text ?>
        </div>
        <div class="row">
            <div class="col-lg-auto">
                <?= $feedback->date ?>
            </div>
            <div class="col-lg-9"></div>
            <div class="col-lg-auto">
                <?= Html::a($feedback->like, ['/site/like-in-show', 'id' => Html::encode($feedback->id)])?>
            </div>
        </div>
        <div class="row">
            <?php if($feedbackImg->path !== null): ?>
                <img src="<?= $feedbackImg->path?>" alt="">
            <?php endif; ?>
        </div>
        <?php Pjax::begin()?>
        <div class="container">
            <?php Pjax::begin() ?>
            <?php foreach ($comments as $note): ?>
                <div class="container">
                    <div class="row">
                        <?= User::findById($note->userId)->username ?>
                    </div>
                    <div class="row">
                        <?= $note->commentText ?>
                    </div>
                    <div class="row">
                        <?= Html::a($note->like, ['/site/like-comment', 'id' => Html::encode($note->id)])?>
                    </div>
                    <div class="row">
                        <?php $commentImg = CommentImg::getCommentImgById($note->id)?>
                        <?php if($commentImg->path !== null): ?>
                            <img src="<?= $commentImg->path?>" alt="">
                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach ?>
            <?php Pjax::end() ?>
        </div>
        <?php if (Yii::$app->user->id == $feedback->userId || Yii::$app->user->id == 3): ?>
            <div class="col-12">
                <?php $form = ActiveForm::begin([
                    'id' => 'form-comment',
                    'layout' => 'horizontal',
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                        'labelOptions' => ['class' => 'col-lg-1 col-form-label'],
                    ],
                    'options' => ['enctype' => 'multipart/form-data'],
                ])
                ?>

                <?= $form->field($comment, 'username')->textInput([
                    'maxlength' => true,
                    'value' => Yii::$app->user->identity->username,
                    'readonly' => true,
                ]) ?>


                <?= $form->field($comment, 'commentText')->textarea(['rows' => 5]) ?>

                <?= $form->field($comment, 'image')->fileInput() ?>

                <div class="form-group">
                    <?= Html::submitButton('Leave a comment', ['class' => 'btn btn-success']) ?>
                </div>
                <?php ActiveForm::end() ?>
            </div>
        <?php endif ?>
    </div>
</div>

<?php
/*$js = <<<JS
        $('.btn').on(function () {
            var data = $(this).serialize();
            $.ajax({
                url: '/site/show-feedback',
                type: 'POST',
                data: {'data': data},
                success: function (res) {
                    alert('Save comment');
                },
                error: function () {
                    alert('Error!');
                }
            });
            return false;
        });
JS;

$this->registerJs($js);*/
?>
